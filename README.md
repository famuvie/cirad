# Cirad institutional LaTeX package


This LaTeX package ships Cirad's institutional logos and colors.
It also includes a class file based on KOMAscript's `scrartcl` that uses `fancyhdr` to include the logos in the pages.

## Installation

Download and uncompress somewhere, and then type `make` from the command line.

## Example

![example](https://i.imgur.com/CyGqhbR.png)

## License

Copyright (c) Facundo Muñoz 2017

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3c
of this license or (at your option) any later version.
The latest version of this license is in
http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.
