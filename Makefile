.PHONY: install install_zip cleanup

install: 
	mkdir -p ~/texmf
	cp -a doc source tex ~/texmf

install_zip: cirad.tds.zip
	unzip -u $< -d ~/texmf/

cleanup:
	rm -f cirad.tds.zip

cirad.tds.zip:
	zip -r $@ doc/ source/ tex/

